import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { StoresComponent } from './vistas/stores/stores.component';
import { StoresAddComponent } from './vistas/stores-add/stores-add.component';
import { StoresEditComponent } from './vistas/stores-edit/stores-edit.component';
import { ArticlesComponent } from './vistas/articles/articles.component';
import { ArticlesAddComponent } from './vistas/articles-add/articles-add.component';
import { ArticlesEditComponent } from './vistas/articles-edit/articles-edit.component';

const routes: Routes = [
  { path:'' , redirectTo:'dashboard' , pathMatch:'full' },
  { path:'dashboard' , component:DashboardComponent },
  { path:'stores' , component:StoresComponent },
  { path:'storesadd' , component:StoresAddComponent },
  { path:'storesedit/:id' , component:StoresEditComponent },
  { path:'articles' , component:ArticlesComponent },
  { path:'articlesadd' , component:ArticlesAddComponent },
  { path:'articlesedit/:id' , component:ArticlesEditComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  DashboardComponent, 
  StoresComponent, 
  StoresAddComponent, 
  StoresEditComponent, 
  ArticlesComponent, 
  ArticlesAddComponent, 
  ArticlesEditComponent]
