export interface ArticleI{
    id: number | null;
    name: string;
    description: string;
    price: number;
    total_in_shelf: number;
    total_in_vault: number;
    store_id: number;
}
