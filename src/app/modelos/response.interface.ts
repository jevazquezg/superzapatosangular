import { ArticleI } from "./article.interface";
import { StoreI } from "./store.interface";

export interface ResponseI{
    success: boolean;
    error_code: number;
    error_mesg: string;
    article: ArticleI;
    articles: ArticleI[];
    store: StoreI;
    stores: StoreI[];
    total_elements: number;
}