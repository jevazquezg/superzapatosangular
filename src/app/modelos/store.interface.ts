export interface StoreI{
    id: number | null;
    name: string;
    address: string;
}
