import { Injectable } from '@angular/core';

import { ResponseI } from '../../modelos/response.interface';
import { ArticleI } from '../../modelos/article.interface';
import { StoreI } from '../../modelos/store.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  url: string = "https://localhost:44344/services";

  constructor(private http:HttpClient) { }

  getAllStores():Observable<ResponseI>{
    let direccion = this.url + "/stores";
    return this.http.get<ResponseI>(direccion);
  }

  getStoreById(id : number):Observable<ResponseI>{
    let direccion = this.url + "/store/" + id;
    return this.http.get<ResponseI>(direccion);
  }

  setStore(store : Object):Observable<ResponseI>{
    let direccion = this.url + "/store";
    return this.http.post<ResponseI>(direccion, store);
  }

  delStore(id : number):Observable<ResponseI>{
    let direccion = this.url + "/store/" + id;
    return this.http.delete<ResponseI>(direccion);
  }

  
  getAllArticles():Observable<ResponseI>{
    let direccion = this.url + "/articles";
    return this.http.get<ResponseI>(direccion);
  }

  getArticleById(id: number):Observable<ResponseI>{
    let direccion = this.url + "/article/" + id;
    return this.http.get<ResponseI>(direccion);
  }

  setArticle(article : object):Observable<ResponseI>{
    let direccion = this.url + "/article";
    return this.http.post<ResponseI>(direccion, article);
  }

  delArticle(id : number):Observable<ResponseI>{
    let direccion = this.url + "/article/" + id;
    return this.http.delete<ResponseI>(direccion);
  }

}
