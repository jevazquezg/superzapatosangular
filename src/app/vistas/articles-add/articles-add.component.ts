import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormControlName, Form } from '@angular/forms';
import { ArticleI } from '../../modelos/article.interface';
import { ResponseI } from '../../modelos/response.interface';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-articles-add',
  templateUrl: './articles-add.component.html',
  styleUrls: ['./articles-add.component.css']
})
export class ArticlesAddComponent implements OnInit {

  articleForm = new FormGroup({
    id : new FormControl(),
    name : new FormControl('', Validators.required),
    description : new FormControl(''),
    price : new FormControl(),
    total_in_shelf : new FormControl(),
    total_in_vault : new FormControl(),
    store_id : new FormControl()
  });

  response: ResponseI;
  article: ArticleI;

  constructor(private api:ApiService, private alertas:AlertasService) { }

  ngOnInit(): void {
  }

  saveArticle(form : FormGroup){

    let myObj = {
      id: null,
      name: String(form.controls['name'].value),
      description: String(form.controls['description'].value),
      price: Number(form.controls['price'].value),
      total_in_shelf: Number(form.controls['total_in_shelf'].value),
      total_in_vault: Number(form.controls['total_in_vault'].value),
      store_id: Number(form.controls['store_id'].value)
    };

    this.api.setArticle(myObj).subscribe(data => {
        let respuesta : ResponseI = data;

        if (respuesta.success) {
          this.alertas.showSuccess('Registro guardado de forma correcta', 'Super Zapatos');
        } else {
          this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
        }
    })
  }

}
