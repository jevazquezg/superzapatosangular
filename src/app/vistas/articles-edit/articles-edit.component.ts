import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormControlName, Form } from '@angular/forms';
import { ArticleI } from '../../modelos/article.interface';
import { ResponseI } from '../../modelos/response.interface';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-articles-edit',
  templateUrl: './articles-edit.component.html',
  styleUrls: ['./articles-edit.component.css']
})
export class ArticlesEditComponent implements OnInit {
  
  articleForm = new FormGroup({
    id : new FormControl(),
    name : new FormControl('', Validators.required),
    description : new FormControl(''),
    price : new FormControl(),
    total_in_shelf : new FormControl(),
    total_in_vault : new FormControl(),
    store_id : new FormControl()
  });

  response: ResponseI;
  article: ArticleI;

  constructor(private api:ApiService, private alertas:AlertasService, private activeroute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let articleId = Number(this.activeroute.snapshot.paramMap.get('id'))

    this.api.getArticleById(articleId).subscribe(data => {
      this.response = data;
      this.article = this.response.article;

      this.articleForm.controls['id'].setValue(data.article.id);
      this.articleForm.controls['name'].setValue(data.article.name);
      this.articleForm.controls['description'].setValue(data.article.description);
      this.articleForm.controls['price'].setValue(data.article.price);
      this.articleForm.controls['total_in_shelf'].setValue(data.article.total_in_shelf);
      this.articleForm.controls['total_in_vault'].setValue(data.article.total_in_vault);
      this.articleForm.controls['store_id'].setValue(data.article.store_id);
    })
  }

  saveArticle(form : FormGroup){

    let myObj = {
      id: Number(form.controls['id'].value),
      name: String(form.controls['name'].value),
      description: String(form.controls['description'].value),
      price: Number(form.controls['price'].value),
      total_in_shelf: Number(form.controls['total_in_shelf'].value),
      total_in_vault: Number(form.controls['total_in_vault'].value),
      store_id: Number(form.controls['store_id'].value)
    };
    
    this.api.setArticle(myObj).subscribe(data => {
        let respuesta : ResponseI = data;

        if (respuesta.success) {
          this.alertas.showSuccess('Registro guardado de forma correcta', 'Super Zapatos');
        } else {
          this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
        }
    })
  }

  deleteArticle(){
    let articleId: Number = this.articleForm.controls['id'].value;

    this.api.delArticle(Number(articleId)).subscribe(data => {
      let respuesta : ResponseI = data;

      if (respuesta.success) {
        this.alertas.showSuccess('Registro borrado de forma correcta', 'Super Zapatos');
        this.router.navigate(['articles']);
      } else {
        this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
      }
    })

  }

}
