import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../servicios/api/api.service';
import { Router } from '@angular/router';

import { ArticleI } from 'src/app/modelos/article.interface';
import { ResponseI } from '../../modelos/response.interface';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  response: ResponseI | undefined;
  articles: ArticleI[] | undefined;

  constructor(private api:ApiService, private router:Router) { }

  ngOnInit(): void {
    this.api.getAllArticles().subscribe(data => {
      this.response = data;
      this.articles = this.response.articles;
    })
  }

  editArticle(id: number | null){
    this.router.navigate(['articlesedit', id]);
  }

  addArticle(){
    this.router.navigate(['articlesadd']);
  }

}
