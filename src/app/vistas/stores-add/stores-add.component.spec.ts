import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoresAddComponent } from './stores-add.component';

describe('StoresAddComponent', () => {
  let component: StoresAddComponent;
  let fixture: ComponentFixture<StoresAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoresAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StoresAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
