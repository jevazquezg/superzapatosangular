import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormControlName, Form } from '@angular/forms';
import { StoreI } from '../../modelos/store.interface';
import { ResponseI } from '../../modelos/response.interface';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';

@Component({
  selector: 'app-stores-add',
  templateUrl: './stores-add.component.html',
  styleUrls: ['./stores-add.component.css']
})
export class StoresAddComponent implements OnInit {

  storeForm = new FormGroup({
    id : new FormControl(),
    name : new FormControl('', Validators.required),
    address : new FormControl('')
  });

  response: ResponseI;
  store: StoreI;

  constructor(private api:ApiService, private alertas:AlertasService) { }

  ngOnInit(): void {
  }

  saveStore(form : FormGroup){

    let myObj = { 
      id: null,
      name: String(form.controls['name'].value),
      address: String(form.controls['address'].value)
    };

    this.api.setStore(myObj).subscribe(data => {
        let respuesta : ResponseI = data;

        if (respuesta.success) {
          this.alertas.showSuccess('Registro guardado de forma correcta', 'Super Zapatos');
        } else {
          this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
        }
    })
  }
}
