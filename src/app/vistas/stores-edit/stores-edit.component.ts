import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormControlName, Form } from '@angular/forms';
import { StoreI } from '../../modelos/store.interface';
import { ResponseI } from '../../modelos/response.interface';
import { ApiService } from 'src/app/servicios/api/api.service';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-stores-edit',
  templateUrl: './stores-edit.component.html',
  styleUrls: ['./stores-edit.component.css']
})
export class StoresEditComponent implements OnInit {

  storeForm = new FormGroup({
    id : new FormControl(),
    name : new FormControl('', Validators.required),
    address : new FormControl('')
  });

  response: ResponseI | undefined;
  store: StoreI | undefined;

  constructor(private api:ApiService, private alertas:AlertasService, private activeroute: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    let storeId = Number(this.activeroute.snapshot.paramMap.get('id'))

    this.api.getStoreById(storeId).subscribe(data => {
      this.response = data;
      this.store = this.response.store;

      this.storeForm.controls['id'].setValue(data.store.id);
      this.storeForm.controls['name'].setValue(data.store.name);
      this.storeForm.controls['address'].setValue(data.store.address);
    })
  }

  saveStore(form : FormGroup){

    let myObj = { 
      id: Number(form.controls['id'].value),
      name: String(form.controls['name'].value),
      address: String(form.controls['address'].value)
    };
    
    this.api.setStore(myObj).subscribe(data => {
        let respuesta : ResponseI = data;

        if (respuesta.success) {
          this.alertas.showSuccess('Registro guardado de forma correcta', 'Super Zapatos');
        } else {
          this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
        }
    })
  }

  deleteStore(){
    let storeId: Number = this.storeForm.controls['id'].value;

    this.api.delStore(Number(storeId)).subscribe(data => {
      let respuesta : ResponseI = data;

      if (respuesta.success) {
        this.alertas.showSuccess('Registro borrado de forma correcta', 'Super Zapatos');
        this.router.navigate(['stores']);
      } else {
        this.alertas.showError('No se pudo completar la acción', 'Super Zapatos');
      }
    })

  }

}
