import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../servicios/api/api.service';
import { Router } from '@angular/router';

import { StoreI } from '../../modelos/store.interface';
import { ResponseI } from '../../modelos/response.interface';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {

  response: ResponseI | undefined;
  stores: StoreI[] | undefined;

  constructor(private api:ApiService, private router:Router) { }

  ngOnInit(): void {
    this.api.getAllStores().subscribe(data => {
      this.response = data;
      this.stores = this.response.stores;
    })
  }

  editStore(id: number | null){
    this.router.navigate(['storesedit', id]);
  }

  addStore(){
    this.router.navigate(['storesadd']);
  }

}
